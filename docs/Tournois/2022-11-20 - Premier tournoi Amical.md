# Premier tournoi amical, 20 novembre 2022

**de Lord of the Ring Strategy Battle Game du Détour Ludique!**

- Dimanche 20 novembre 2022
- 500 points
- 12 places
- 10$
- 3 rondes
- Ouverture des portes à 9h et début du premier match à 9h30

Inscriptions sur la section événement du site du Détour Ludique
https://www.detourludique.com/evenements


## Résultats
