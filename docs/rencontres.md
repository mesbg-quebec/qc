---
sidebar_position: 1
---

# Rencontres

## Un mercredi sur deux

- Au détour ludique / La chope Gobline
- Souvant 500pts, en simple ou en double
- Annoncez-vous dans le [groupe Facbook](https://www.facebook.com/groups/quebec.mesbg)
- Les participants arrivent souvent entre 17-18h
