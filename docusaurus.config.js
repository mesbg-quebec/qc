// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require("prism-react-renderer/themes/github");
const darkCodeTheme = require("prism-react-renderer/themes/dracula");

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: "Québec - Middle-earth SBG",
  tagline: "Jeu de bataille stratégique",
  url: "https://mesbg-quebec.gitlab.io",
  baseUrl: "/qc",
  onBrokenLinks: "throw",
  onBrokenMarkdownLinks: "warn",
  favicon: "img/favicon.ico",

  // GitHub pages deployment config.
  // If you aren't using GitHub pages, you don't need these.
  organizationName: "pieplu", // Usually your GitHub org/user name.
  projectName: "qc", // Usually your repo name.

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: "fr",
    locales: ["fr"],
  },

  presets: [
    [
      "classic",
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve("./sidebars.js"),
        },
        blog: {
          showReadingTime: true,
        },
        theme: {
          customCss: require.resolve("./src/css/custom.css"),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      navbar: {
        title: "Middle-earth Québec",
        logo: {
          alt: "Logo, inscription elfique de l'anneau unique",
          src: "img/the-one-ring.svg",
        },
        items: [
          {
            type: "doc",
            docId: "rencontres",
            position: "left",
            label: "Rencontres",
          },
          {
            href: "https://www.facebook.com/groups/quebec.mesbg",
            label: "Groupe Facebook",
            position: "right",
          },
        ],
      },
      footer: {
        style: "dark",
        links: [
          {
            title: "Informations",
            items: [
              {
                label: "Rencontres",
                to: "/docs/rencontres",
              },
              {
                label: "Liens utiles",
                to: "/docs/trucs-et-astuces/liens",
              },
            ],
          },
          {
            title: "Communauté",
            items: [
              {
                label: "Groupe Facebook",
                href: "https://www.facebook.com/groups/quebec.mesbg",
              },
            ],
          },
          {
            title: "Autres groupes",
            items: [
              {
                label: "Montréal",
                href: "http://middleearthmontreal.com",
              },
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()}`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = config;
